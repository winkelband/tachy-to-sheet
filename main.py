#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Die Variable 'master' liest stdin (input file @ oneliner).

import csv
import sys

file_paths = sys.argv[1:]
for name in file_paths:    
    master = list(csv.reader(open(name, 'rb'), delimiter='\t'))

    for i in range(1, len(master)):
        with open('Fläche_'.decode('utf8') + master[i][4] + '_Pl_' + master[i][5] + '_' + master[i][7] + '.txt', 'a') as csvfile:
            filewriter = csv.writer(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
            filewriter.writerow(master[i])


